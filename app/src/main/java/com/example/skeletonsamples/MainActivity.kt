package com.example.skeletonsamples

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.skeletonsamples.skeleton.SkeletonController
import com.example.skeletonsamples.skeleton.model.VehicleModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var skeletonController: SkeletonController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dataModeButton.setOnClickListener(this)
        inspectionStateButton.setOnClickListener(this)

        skeletonController = SkeletonController(VehicleModel.createTruckStub())
        mainContainer.addView(skeletonController.buildVehicleView(this))
    }


    override fun onClick(view: View?) {
        when (view) {
            dataModeButton -> {
                skeletonController.swapDataMode()
            }
        }
    }

}

