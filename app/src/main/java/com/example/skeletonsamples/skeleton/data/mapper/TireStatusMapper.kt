package com.example.skeletonsamples.skeleton.data.mapper

import com.example.skeletonsamples.core.data.Mapper
import com.example.skeletonsamples.skeleton.data.entity.TyreStatusEntity
import com.example.skeletonsamples.skeleton.model.TireStatus

class TireStatusMapper : Mapper<TyreStatusEntity, TireStatus> {

    override fun mapSingle(source: TyreStatusEntity): TireStatus = when (source.code) {
        "N" -> TireStatus.NEW
        else -> TireStatus.PART_WORN
    }
}