package com.example.skeletonsamples.skeleton.model

enum class VehicleDisplayType {
    VEHICLE,
    TRAILER
}