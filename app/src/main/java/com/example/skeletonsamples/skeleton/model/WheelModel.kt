package com.example.skeletonsamples.skeleton.model

data class WheelModel(
    val wheelId: String,
    val wheelType: String,
    val number: Int,
    var displayLabel: String = "",
    var displayMode: DisplayMode = DisplayMode.GRAPHICAL,
    var inspectionState: InspectionState = InspectionState.NON_INSPECTED,
    var tire: TireModel? = null
) {

    val hasNoTire: Boolean
        get() = tire == null
}