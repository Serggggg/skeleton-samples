package com.example.skeletonsamples.skeleton.model

enum class MaintainState {
    NON_MAINTAINED,
    MAINTAINED
}