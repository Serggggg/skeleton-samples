package com.example.skeletonsamples.skeleton.view

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import com.example.skeletonsamples.core.extensions.*
import com.example.skeletonsamples.skeleton.model.*

/**
 * ATTENTION!
 * This View does not inflate layout resource.
 * It builds dynamically, according to [AxleModel]
 */
class AxleView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var verticalAxleLineViewTop: VerticalAxleLineView? = null
    private var verticalAxleLineViewBottom: VerticalAxleLineView? = null
    private var horizontalAxleAnchorLine: View? = null
    var axleLineView: AxleLineView? = null

    var topVerticalAxleDisplayMode: VerticalAxleLineView.DisplayMode =
        VerticalAxleLineView.DisplayMode.VISIBLE
    var bottomVerticalAxleDisplayMode: VerticalAxleLineView.DisplayMode =
        VerticalAxleLineView.DisplayMode.VISIBLE

    private val wheelsLeft: MutableList<WheelView> = mutableListOf()
    private val wheelsRight: MutableList<WheelView> = mutableListOf()

    init {
        layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
    }

    private fun addAxleLines(axle: AxleModel) {
        // Vertical
        verticalAxleLineViewTop = VerticalAxleLineView(context).also {
            it.displayMode = topVerticalAxleDisplayMode
            addView(it)
        }
        verticalAxleLineViewBottom = VerticalAxleLineView(context).also {
            it.displayMode = bottomVerticalAxleDisplayMode
            addView(it)
        }
        // Horizontal
        horizontalAxleAnchorLine = View(context).also { line ->
            line.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, 1)
            line.setBackgroundColor(Color.TRANSPARENT)
            line.id = View.generateViewId()
            addView(line)
        }
        axleLineView = AxleLineView(context)
            .also { axleLine ->
                axleLine.bindAxleData(axle)
                addView(axleLine)
            }
    }

    private fun addWheels(
        wheels: List<WheelModel>,
        targetWheelViews: MutableList<WheelView>,
        axleViewsMap: MutableMap<String, WheelView>? = null
    ) {
        wheels.forEach { wheelModel ->
            WheelView(context).apply {
                layoutParams =
                    LayoutParams(context.dpToPx(64), context.dpToPx(124))
                labelView = WheelLabelView(context).also { this@AxleView.addView(it) }
                updateViewData(wheelModel)
                targetWheelViews.add(this)
                this@AxleView.addView(this)

                axleViewsMap?.put(wheelModel.wheelId, this)
            }
        }
    }

    private fun connectWheelAndLabel(
        constraintSet: ConstraintSet,
        wheelView: WheelView,
        labelView: WheelLabelView
    ) {
        with(constraintSet) {
            labelView.alignWidthTo(this, wheelView)
            wheelView.putToBottomOf(this, labelView)
        }
    }

    private fun buildConstraints() {
        val constraintSet = ConstraintSet().apply { clone(this@AxleView) }

        // Left side
        wheelsLeft.forEachIndexed { index, wheelView ->
            with(wheelView) {
                if (index == 0)
                    connectToParentStart(constraintSet)
                else
                    putToEndOf(constraintSet, wheelsLeft[index - 1], context.dpToPx(8))
                labelView?.let { labelView ->
                    labelView.connectToParentTop(constraintSet)
                    connectWheelAndLabel(constraintSet, wheelView, labelView)
                }
            }
        }

        // Right side
        wheelsRight.forEachIndexed { index, wheelView ->
            with(wheelView) {
                if (index == 0)
                    connectToParentEnd(constraintSet)
                else
                    putToStartOf(constraintSet, wheelsRight[index - 1], context.dpToPx(8))
                labelView?.let { labelView ->
                    labelView.connectToParentTop(constraintSet)
                    connectWheelAndLabel(constraintSet, wheelView, labelView)
                }
            }
        }

        // Horizontal anchor
        wheelsLeft.firstOrNull()?.let {
            horizontalAxleAnchorLine?.alignHeightTo(constraintSet, it)
        }

        // Vertical Axles
        verticalAxleLineViewTop?.apply {
            horizontalAxleAnchorLine?.let {
                centerInParentHorizontal(constraintSet)
                connectToParentTop(constraintSet)
                putToTopOf(constraintSet, it)
            }
        }
        verticalAxleLineViewBottom?.apply {
            horizontalAxleAnchorLine?.let {
                centerInParentHorizontal(constraintSet)
                connectToParentBottom(constraintSet)
                putToBottomOf(constraintSet, it)
            }
        }

        // Horizontal AxleEntity line
        wheelsLeft.firstOrNull()?.let {
            axleLineView?.alignHeightTo(constraintSet, it)
        }

        constraintSet.applyTo(this)
    }

    fun build(axle: AxleModel, axleViewsMap: MutableMap<String, WheelView>? = null) {
        addAxleLines(axle)
        addWheels(axle.wheelsLeftSide, wheelsLeft, axleViewsMap)
        addWheels(axle.wheelsRightSide, wheelsRight, axleViewsMap)
        buildConstraints()
    }

}