package com.example.skeletonsamples.skeleton.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.example.skeletonsamples.R
import com.example.skeletonsamples.core.extensions.toggleView
import com.example.skeletonsamples.skeleton.model.*
import kotlinx.android.synthetic.main.layout_wheel_view.view.*
import java.util.*

class WheelView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {

    var labelView: WheelLabelView? = null

    init {
        LayoutInflater.from(context).inflate(R.layout.layout_wheel_view, this)
        id = View.generateViewId()
        setOnClickListener { Toast.makeText(context, "Wheel clicked", Toast.LENGTH_SHORT).show() }
    }

    private fun setWheelTextLabelsVisible(visible: Boolean) {
        wheelSizeTextView.toggleView(visible)
        wheelSizeUnitsTextView.toggleView(visible)
        wheelPressureTextView.toggleView(visible)
        wheelPressureUnitsTextView.toggleView(visible)
    }

    private fun updateDataLabels(wheel: WheelModel) {
        wheel.tire?.let { tire ->
            wheelSizeTextView.text = tire.sizeMm.toString()
            wheelPressureTextView.text =
                String.format(Locale.getDefault(), "%.1f", tire.pressureBar)
        }
    }

    private fun recognizeWheelBackground(wheel: WheelModel): Int {

        if (wheel.hasNoTire)
            return BKG_TIRE_DISMOUNTED

        return when (wheel.displayMode) {
            DisplayMode.DATA -> {
                when (wheel.inspectionState) {
                    InspectionState.NON_INSPECTED -> BKG_TIRE_NON_INSPECTED_DATA_MODE
                    InspectionState.INSPECTED -> BKG_TIRE_INSPECTED_DATA_MODE
                }
            }
            DisplayMode.GRAPHICAL -> {
                when (wheel.inspectionState) {
                    InspectionState.NON_INSPECTED -> {
                        when (wheel.tire?.maintainState) {
                            MaintainState.NON_MAINTAINED -> BKG_TIRE_NON_INSPECTED_NOT_MAINTAINED
                            MaintainState.MAINTAINED -> BKG_TIRE_NON_INSPECTED
                            null -> BKG_TIRE_DISMOUNTED
                        }
                    }
                    InspectionState.INSPECTED -> {
                        when (wheel.tire?.maintainState) {
                            MaintainState.NON_MAINTAINED -> BKG_TIRE_INSPECTED_NOT_MAINTAINED
                            MaintainState.MAINTAINED -> {
                                when (wheel.tire?.status) {
                                    TireStatus.NEW -> BKG_TIRE_INSPECTED_NEW
                                    TireStatus.PART_WORN -> BKG_TIRE_INSPECTED_PART_WORN
                                    null -> BKG_TIRE_DISMOUNTED
                                }
                            }
                            null -> BKG_TIRE_DISMOUNTED
                        }
                    }
                }
            }
        }
    }

    fun updateViewData(wheelModel: WheelModel) {
        background = ContextCompat.getDrawable(context, recognizeWheelBackground(wheelModel))
        updateDataLabels(wheelModel)
        setWheelTextLabelsVisible(!wheelModel.hasNoTire && wheelModel.displayMode == DisplayMode.DATA)
        labelView?.apply {
            setLabelTitle(wheelModel.displayLabel)
            setInspectionState(wheelModel.inspectionState)
        }
    }

    @Deprecated("Not in use")
    fun createAndLaunchAnimation() {
        val ani = AlphaAnimation(1F, 0.3F).apply {
            repeatCount = Animation.INFINITE
            repeatMode = Animation.REVERSE
            interpolator = LinearInterpolator()
            duration = 400L
        }
        startAnimation(ani)
    }

    companion object {

        private const val BKG_TIRE_DISMOUNTED = R.drawable.ic_tire_dismounted
        private const val BKG_TIRE_NON_INSPECTED_DATA_MODE =
            R.drawable.ic_tire_non_inspected_data_mode
        private const val BKG_TIRE_INSPECTED_DATA_MODE = R.drawable.ic_tire_inspected_data_mode
        private const val BKG_TIRE_NON_INSPECTED_NOT_MAINTAINED =
            R.drawable.ic_tire_non_inspected_not_maintained
        private const val BKG_TIRE_NON_INSPECTED = R.drawable.ic_tire_non_inspected
        private const val BKG_TIRE_INSPECTED_NOT_MAINTAINED =
            R.drawable.ic_tire_inspected_not_maintained
        private const val BKG_TIRE_INSPECTED_NEW = R.drawable.ic_tire_inspected_new
        private const val BKG_TIRE_INSPECTED_PART_WORN = R.drawable.ic_tire_inspected_part_worn
    }

}