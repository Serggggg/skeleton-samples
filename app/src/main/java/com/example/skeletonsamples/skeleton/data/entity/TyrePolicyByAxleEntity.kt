package com.example.skeletonsamples.skeleton.data.entity

import com.google.gson.annotations.SerializedName

data class TyrePolicyByAxleEntity(
    @SerializedName("policyInfo")
    val policyInfoList: List<PolicyInfoEntity> = listOf(),
    @SerializedName("policyNumber")
    val policyNumber: String?
)