package com.example.skeletonsamples.skeleton.data.entity

import com.google.gson.annotations.SerializedName

data class GeneralPolicyInfoEntity(
    @SerializedName("hasBreakdownCoverage")
    val hasBreakdownCoverage: Boolean?,
    @SerializedName("isHpValveCapAllowed")
    val isHpValveCapAllowed: Boolean?,
    @SerializedName("isValveExtensionAllowed")
    val isValveExtensionAllowed: Boolean?,
    @SerializedName("isBalancingAllowed")
    val isBalancingAllowed: Boolean?,
    @SerializedName("isGeometriccorrectionAllowed")
    val isGeometricCorrectionAllowed: Boolean?,
    @SerializedName("isGeometricCheckAllowed")
    val isGeometricCheckAllowed: Boolean?,
    @SerializedName("isPressureCheckAllowed")
    val isPressureCheckAllowed: Boolean?,
    @SerializedName("isPressureCorrectionAllowed")
    val isPressureCorrectionAllowed: Boolean?
)