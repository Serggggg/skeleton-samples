package com.example.skeletonsamples.skeleton.model

enum class DisplayMode {
    GRAPHICAL,
    DATA
}