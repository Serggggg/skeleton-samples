package com.example.skeletonsamples.skeleton.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.example.skeletonsamples.R
import com.example.skeletonsamples.core.extensions.disappear
import com.example.skeletonsamples.core.extensions.hide
import com.example.skeletonsamples.core.extensions.show
import kotlinx.android.synthetic.main.layout_vertical_axle_view.view.*

class VerticalAxleLineView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    enum class DisplayMode {
        VISIBLE,
        INVISIBLE,
        TAIL
    }

    var displayMode: DisplayMode = DisplayMode.VISIBLE
    set(value) {
        field = value
        updateDisplayMode()
    }

    init {
        LayoutInflater.from(context).inflate(R.layout.layout_vertical_axle_view, this)
        layoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, 0).apply {
            val horizontalPadding = context.resources
                .getDimension(R.dimen.vertical_axle_horizontal_padding).toInt()
            setPadding(horizontalPadding, 0, horizontalPadding, 0)
        }
        updateDisplayMode()
        id = View.generateViewId()
    }

    private fun updateDisplayMode() {
        when (displayMode) {
            DisplayMode.VISIBLE -> {
                setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.color_trailer_background
                    )
                )
                wheelVerticalAxleLineView.show()
                show()
            }
            DisplayMode.INVISIBLE -> {
                hide()
            }
            DisplayMode.TAIL -> {
                setBackgroundResource(R.drawable.bkg_vertical_axle_tail)
                wheelVerticalAxleLineView.disappear()
                show()
            }
        }
    }
}