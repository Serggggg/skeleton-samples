package com.example.skeletonsamples.skeleton.data.entity

import com.google.gson.annotations.SerializedName

data class PolicyInfoEntity(
    @SerializedName("allowedTyreType")
    val allowedTyreType: AllowedTyreTypeEntity?,
    @SerializedName("isMajorRepairAllowed")
    val isMajorRepairAllowed: Boolean?,
    @SerializedName("isRegrooveAllowed")
    val isRegrooveAllowed: Boolean?,
    @SerializedName("axleType")
    val axleType: String?,
    @SerializedName("isMinorRepairAllowed")
    val isMinorRepairAllowed: Boolean?,
    @SerializedName("allowedBrandCode")
    val allowedBrandCode: String?
)