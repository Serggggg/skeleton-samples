package com.example.skeletonsamples.skeleton.data.entity

import com.google.gson.annotations.SerializedName

data class ContractInfoEntity(
    @SerializedName("contractNumber")
    val contractNumber: String?,
    @SerializedName("distributionChannel")
    val distributionChannel: String?,
    @SerializedName("salesOrganisation")
    val salesOrganisation: String?,
    @SerializedName("fleetScenario")
    val fleetScenario: String?
)