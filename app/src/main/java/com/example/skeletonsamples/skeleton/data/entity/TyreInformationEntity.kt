package com.example.skeletonsamples.skeleton.data.entity

import com.google.gson.annotations.SerializedName

data class TyreInformationEntity(
    @SerializedName("tyreId")
    val tyreId: String?,
    @SerializedName("serialNumber")
    val serialNumber: String?,
    @SerializedName("treadDepthMiddle")
    val treadDepthMiddle: String?,
    @SerializedName("status")
    val tyreStatus: TyreStatusEntity?,
    @SerializedName("isRegrooveAllowed")
    val isRegrooveAllowed: Boolean?,
    @SerializedName("productInfo")
    val productInfo: ProductInfoEntity?
)