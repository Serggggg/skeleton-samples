package com.example.skeletonsamples.skeleton.model

data class VehicleModel(
    val vehicleId: String,
    val vehicleDisplayType: VehicleDisplayType,
    val axles: List<AxleModel>
) {

    companion object {

        fun createTrailerStub(): VehicleModel =
            VehicleModel(
                vehicleId = "1982",
                vehicleDisplayType = VehicleDisplayType.TRAILER,
                axles = listOf(
                    AxleModel.createOrdinaryAxleStub2(),
                    AxleModel.createOrdinaryAxleStub4()
                ))

        fun createTruckStub(): VehicleModel =
            VehicleModel(
                vehicleId = "1405",
                vehicleDisplayType = VehicleDisplayType.VEHICLE,
                axles = listOf(
                    AxleModel.createRotaryAxleStub2(),
                    AxleModel.createOrdinaryAxleStub2(),
                    AxleModel.createDriveAxleStub4()
                ))
    }
}