package com.example.skeletonsamples.skeleton.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.example.skeletonsamples.R
import com.example.skeletonsamples.skeleton.model.AxleModel

abstract class BaseTrailerPartView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    private val axleViews: MutableList<AxleView> = mutableListOf()

    protected lateinit var axlesContainer: ViewGroup
    protected open val defaultAxleStubLineHeight =
        context.resources.getDimension(R.dimen.vertical_axle_stub_line_height).toInt()
    protected open val defaultWheelsVerticalInterval =
        context.resources.getDimension(R.dimen.wheels_vertical_interval).toInt()

    init {
        orientation = VERTICAL
        id = View.generateViewId()
    }

    protected abstract fun determineTopVerticalAxleDisplayMode(isFirst: Boolean, isLast: Boolean): VerticalAxleLineView.DisplayMode
    protected abstract fun determineBottomVerticalAxleDisplayMode(isFirst: Boolean, isLast: Boolean): VerticalAxleLineView.DisplayMode


    private fun addAxle(
        axleModel: AxleModel,
        isFirst: Boolean = false,
        isLast: Boolean = false,
        axleViewsMap: MutableMap<String, WheelView>? = null
    ) {

        AxleView(context).also {

            it.topVerticalAxleDisplayMode = determineTopVerticalAxleDisplayMode(isFirst, isLast)
            it.bottomVerticalAxleDisplayMode = determineBottomVerticalAxleDisplayMode(isFirst, isLast)

            it.build(axleModel, axleViewsMap)

            if (axleViews.size > 0) {
                addVerticalAxleLineStub(defaultWheelsVerticalInterval)
            }

            axlesContainer.addView(it)

            axleViews.add(it)
        }
    }

    protected fun addVerticalAxleLineStub(height: Int = defaultAxleStubLineHeight) {
        axlesContainer.addView(VerticalAxleLineStubView.create(context, height))
    }

    open fun addAxles(
        axles: List<AxleModel>,
        axleViewsMap: MutableMap<String, WheelView>? = null
    ) {
        axles.forEachIndexed { index, axleModel ->
            addAxle(axleModel, index == 0, index == axles.size-1, axleViewsMap)
        }
    }
}