package com.example.skeletonsamples.skeleton.data.entity

import com.google.gson.annotations.SerializedName

data class VehicleEntity(
    @SerializedName("contractInfo")
    val contractInfo: ContractInfoEntity,
    @SerializedName("customerInfo")
    val customerInfo: CustomerInfoEntity,
    @SerializedName("generalPolicyInfo")
    val generalPolicyInfo: GeneralPolicyInfoEntity,
    @SerializedName("tyrePolicyByAxle")
    val tyrePolicyByAxle: TyrePolicyByAxleEntity,
    @SerializedName("vehicleHeader")
    val vehicleHeader: VehicleHeaderEntity,
    @SerializedName("axles")
    val axles: List<AxleEntity> = listOf()
)