package com.example.skeletonsamples.skeleton.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import com.example.skeletonsamples.R
import com.example.skeletonsamples.skeleton.model.AxleModel
import kotlinx.android.synthetic.main.layout_truck_front_view.view.*

class TruckFrontView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : BaseTrailerPartView(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.layout_truck_front_view, this)
        axlesContainer = truckFrontAxlesContainer
    }

    override fun addAxles(
        axles: List<AxleModel>,
        axleViewsMap: MutableMap<String, WheelView>?
    ) {
        super.addAxles(axles, axleViewsMap)
        addVerticalAxleLineStub()
    }

    override fun determineTopVerticalAxleDisplayMode(
        isFirst: Boolean,
        isLast: Boolean
    ): VerticalAxleLineView.DisplayMode =
        if (isFirst)
            VerticalAxleLineView.DisplayMode.INVISIBLE
        else
            VerticalAxleLineView.DisplayMode.VISIBLE

    override fun determineBottomVerticalAxleDisplayMode(
        isFirst: Boolean,
        isLast: Boolean
    ): VerticalAxleLineView.DisplayMode = VerticalAxleLineView.DisplayMode.VISIBLE
}