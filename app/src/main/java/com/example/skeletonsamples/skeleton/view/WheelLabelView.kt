package com.example.skeletonsamples.skeleton.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.example.skeletonsamples.R
import com.example.skeletonsamples.skeleton.model.InspectionState
import kotlinx.android.synthetic.main.layout_wheel_label_view.view.*

class WheelLabelView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    init {
        orientation = VERTICAL
        LayoutInflater.from(context).inflate(R.layout.layout_wheel_label_view, this)
        layoutParams = ConstraintLayout.LayoutParams(
            ConstraintLayout.LayoutParams.WRAP_CONTENT,
            ConstraintLayout.LayoutParams.WRAP_CONTENT
        )
        id = View.generateViewId()
    }

    fun setLabelTitle(title: String) {
        wheelLabelTitleTextView.text = title
    }

    fun setInspectionState(state: InspectionState) {
        val textViewDrawableResId = when (state) {
            InspectionState.NON_INSPECTED -> BKG_RES_ID_NON_INSPECTED
            InspectionState.INSPECTED -> BKG_RES_ID_INSPECTED
        }
        val stickColorResId = when (state) {
            InspectionState.NON_INSPECTED -> COLOR_RES_ID_NON_INSPECTED
            InspectionState.INSPECTED -> COLOR_RES_ID_INSPECTED
        }

        wheelLabelTitleTextView.setBackgroundResource(textViewDrawableResId)
        wheelLabelStickView.setBackgroundColor(ContextCompat.getColor(context, stickColorResId))
    }

    companion object {

        private const val BKG_RES_ID_NON_INSPECTED = R.drawable.bkg_wheel_label_non_inspected
        private const val BKG_RES_ID_INSPECTED = R.drawable.bkg_wheel_label_inspected

        private const val COLOR_RES_ID_NON_INSPECTED = R.color.color_wheel_label_non_inspected
        private const val COLOR_RES_ID_INSPECTED = R.color.color_wheel_label_inspected
    }
}