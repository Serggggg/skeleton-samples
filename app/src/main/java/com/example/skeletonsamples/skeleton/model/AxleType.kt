package com.example.skeletonsamples.skeleton.model

enum class AxleType {
    ORDINARY,
    ROTARY,
    DRIVE
}