package com.example.skeletonsamples.skeleton.data.entity

import com.google.gson.annotations.SerializedName

data class ProductInfoEntity(
    @SerializedName("code")
    val code: String?,
    @SerializedName("description")
    val description: String?,
    @SerializedName("brand")
    val brand: BrandEntity?,
    @SerializedName("sizemm")
    val sizeMm: String?,
    @SerializedName("aspectRatio")
    val aspectRatio: String?,
    @SerializedName("rimDiameter")
    val rimDiameter: String?,
    @SerializedName("fuelClass")
    val fuelClass: String?,
    @SerializedName("gripClass")
    val gripClass: String?,
    @SerializedName("noiseClass")
    val noiseClass: String?,
    @SerializedName("noiseValue")
    val noiseValue: String?
)