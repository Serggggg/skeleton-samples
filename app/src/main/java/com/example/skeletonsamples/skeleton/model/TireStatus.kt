package com.example.skeletonsamples.skeleton.model

enum class TireStatus {
    NEW,
    PART_WORN
}