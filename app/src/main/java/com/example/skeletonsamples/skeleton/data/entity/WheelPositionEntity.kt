package com.example.skeletonsamples.skeleton.data.entity

import com.google.gson.annotations.SerializedName

data class WheelPositionEntity(
    @SerializedName("wheelId")
    val wheelId: String?,
    @SerializedName("type")
    val type: WheelTypeEntity?,
    @SerializedName("number")
    val number: String?,
    @SerializedName("tyreInformation")
    val tyreInformation: TyreInformationEntity?
)