package com.example.skeletonsamples.skeleton.data.entity

import com.google.gson.annotations.SerializedName

data class TyreStatusEntity(
    @SerializedName("code")
    val code: String?
)