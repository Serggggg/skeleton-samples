package com.example.skeletonsamples.skeleton.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.example.skeletonsamples.skeleton.model.AxleModel

class TruckView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    private val truckFrontView: TruckFrontView
    private val truckRearView: TruckRearView

    init {
        orientation = VERTICAL
        layoutParams = LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        id = View.generateViewId()

        truckFrontView = TruckFrontView(context).also { addView(it) }
        truckRearView = TruckRearView(context).also { addView(it) }
    }

    fun addFrontAxles(
        axles: List<AxleModel>,
        axleViewsMap: MutableMap<String, WheelView>? = null
    ) {
        truckFrontView.addAxles(axles, axleViewsMap)
    }

    fun addRearAxles(
        axles: List<AxleModel>,
        axleViewsMap: MutableMap<String, WheelView>? = null
    ) {
        truckRearView.addAxles(axles, axleViewsMap)
    }

}