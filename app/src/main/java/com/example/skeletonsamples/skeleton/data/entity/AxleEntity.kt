package com.example.skeletonsamples.skeleton.data.entity

import com.google.gson.annotations.SerializedName

data class AxleEntity(
    @SerializedName("axleId")
    val axleId: String?,
    @SerializedName("type")
    val type: AxleTypeEntity?,
    @SerializedName("number")
    val number: String?,
    @SerializedName("numberOfTires")
    val numberOfTires: String?,
    @SerializedName("wheelPositions")
    val wheelPositions: List<WheelPositionEntity> = listOf()
)