package com.example.skeletonsamples.skeleton.data.mapper

import com.example.skeletonsamples.core.data.Mapper
import com.example.skeletonsamples.skeleton.data.entity.VehicleEntity
import com.example.skeletonsamples.skeleton.model.VehicleDisplayType
import com.example.skeletonsamples.skeleton.model.VehicleModel

// TODO: Refactor Data -> Map to Domain -> Map to Presentation
/**
 * ATTENTION!
 * Temporary solution: Data entity -> Presentation model
 *
 * NEED TO BE REFACTORED
 */
class VehicleModelMapper(
    private val vehicleDisplayTypeMapper: VehicleDisplayTypeMapper,
    private val axleModelMapper: AxleModelMapper
) : Mapper<VehicleEntity, VehicleModel> {

    override fun mapSingle(source: VehicleEntity): VehicleModel =
        VehicleModel(
            vehicleId = source.vehicleHeader.vehicleId ?: "",
            vehicleDisplayType = source.vehicleHeader.vehicleType?.let {
                vehicleDisplayTypeMapper.mapSingle(it)
            } ?: VehicleDisplayType.TRAILER,
            axles = axleModelMapper.mapList(source.axles)
        )


}