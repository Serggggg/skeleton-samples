package com.example.skeletonsamples.skeleton.model

enum class InspectionState {
    NON_INSPECTED,
    INSPECTED
}