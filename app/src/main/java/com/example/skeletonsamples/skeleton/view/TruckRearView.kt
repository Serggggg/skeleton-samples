package com.example.skeletonsamples.skeleton.view

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import com.example.skeletonsamples.skeleton.model.AxleModel

class TruckRearView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : BaseTrailerPartView(context, attrs, defStyleAttr) {

    init {
        layoutParams = LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        axlesContainer = this
    }

    override fun addAxles(
        axles: List<AxleModel>,
        axleViewsMap: MutableMap<String, WheelView>?
    ) {
        addVerticalAxleLineStub()
        super.addAxles(axles, axleViewsMap)
    }

    override fun determineTopVerticalAxleDisplayMode(
        isFirst: Boolean,
        isLast: Boolean
    ): VerticalAxleLineView.DisplayMode = VerticalAxleLineView.DisplayMode.VISIBLE

    override fun determineBottomVerticalAxleDisplayMode(
        isFirst: Boolean,
        isLast: Boolean
    ): VerticalAxleLineView.DisplayMode =
        if (isLast)
            VerticalAxleLineView.DisplayMode.TAIL
        else
            VerticalAxleLineView.DisplayMode.VISIBLE
}