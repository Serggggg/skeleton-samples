package com.example.skeletonsamples.skeleton.data.mapper

import com.example.skeletonsamples.core.data.Mapper
import com.example.skeletonsamples.skeleton.data.entity.TyreInformationEntity
import com.example.skeletonsamples.skeleton.model.MaintainState

class MaintainStateMapper : Mapper<TyreInformationEntity, MaintainState> {

    override fun mapSingle(source: TyreInformationEntity): MaintainState =
        if (source.serialNumber == null)
            MaintainState.NON_MAINTAINED else MaintainState.MAINTAINED
}