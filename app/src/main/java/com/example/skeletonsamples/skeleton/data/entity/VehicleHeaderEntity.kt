package com.example.skeletonsamples.skeleton.data.entity

import com.google.gson.annotations.SerializedName

data class VehicleHeaderEntity(
    @SerializedName("vehicleId")
    val vehicleId: String?,
    @SerializedName("licensePlate")
    val licensePlate: String?,
    @SerializedName("chassisNumber")
    val chassisNumber: String?,
    @SerializedName("manufacturer")
    val manufacturer: String?,
    @SerializedName("model")
    val model: String?,
    @SerializedName("currentOdometer")
    val currentOdometer: String?,
    @SerializedName("unitOfDistance")
    val unitOfDistance: String?,
    @SerializedName("tyrePolicyNumber")
    val tyrePolicyNumber: String?,
    @SerializedName("isOdometerFitted")
    val isOdometerFitted: Boolean?,
    @SerializedName("vehicleType")
    val vehicleType: VehicleTypeEntity?,
    @SerializedName("fleetDepot")
    val fleetDepot: FleetDepotEntity?,
    @SerializedName("scheduleInspectionDueDate")
    val scheduleInspectionDueDate: String?,
    @SerializedName("previousInspectionDate")
    val previousInspectionDate: String?,
    @SerializedName("isDifferentSizeAllowed")
    val isDifferentSizeAllowed: Boolean?,
    @SerializedName("isTpmsFitted")
    val isTpmsFitted: Boolean?
)