package com.example.skeletonsamples.skeleton.data.mapper

import com.example.skeletonsamples.core.data.Mapper
import com.example.skeletonsamples.skeleton.data.entity.AxleEntity
import com.example.skeletonsamples.skeleton.model.AxleModel
import com.example.skeletonsamples.skeleton.model.AxleType

// TODO: Refactor to Mapper<DomainLayer.Axle, AxleModel>
class AxleModelMapper(
    private val axleTypeMapper: AxleTypeMapper,
    private val wheelModelMapper: WheelModelMapper
) : Mapper<AxleEntity, AxleModel> {

    override fun mapSingle(source: AxleEntity): AxleModel =
        AxleModel(
            axleId = source.axleId ?: "",
            axleNumber = source.number?.trim()?.toInt() ?: 0,
            axleType = source.type?.let { axleTypeMapper.mapSingle(it) } ?: AxleType.ORDINARY,
            wheels = wheelModelMapper.mapList(source.wheelPositions)
        )
}