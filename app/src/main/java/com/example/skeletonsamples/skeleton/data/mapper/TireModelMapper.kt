package com.example.skeletonsamples.skeleton.data.mapper

import com.example.skeletonsamples.core.data.Mapper
import com.example.skeletonsamples.skeleton.data.entity.TyreInformationEntity
import com.example.skeletonsamples.skeleton.model.TireModel

class TireModelMapper(
    private val tireStatusMapper: TireStatusMapper,
    private val maintainStateMapper: MaintainStateMapper
) : Mapper<TyreInformationEntity, TireModel> {

    override fun mapSingle(source: TyreInformationEntity): TireModel =
        TireModel(
            tireId = source.tyreId ?: "",
            serial = source.serialNumber ?: "",
            status = source.tyreStatus?.let { tireStatusMapper.mapSingle(it) },
            maintainState = maintainStateMapper.mapSingle(source),
            treadDepthMiddle = source.treadDepthMiddle?.trim()?.toFloat() ?: 0F,
            isRegrooveAllowed = source.isRegrooveAllowed ?: false,
            productCode = source.productInfo?.code ?: "",
            productDescription = source.productInfo?.description ?: "",
            sizeMm = source.productInfo?.sizeMm?.trim()?.toInt() ?: 0,
            aspectRatio = source.productInfo?.aspectRatio?.trim()?.toFloat() ?: 0F,
            rimDiameter = source.productInfo?.rimDiameter?.trim()?.toFloat() ?: 0F,
            fuelClass = source.productInfo?.fuelClass ?: "",
            gripClass = source.productInfo?.gripClass ?: "",
            noiseClass = source.productInfo?.noiseClass?.trim()?.toInt() ?: 0,
            noiseValue = source.productInfo?.noiseValue?.trim()?.toFloat() ?: 0F
            // pressureBar has to be added
        )
}