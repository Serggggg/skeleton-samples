package com.example.skeletonsamples.skeleton.data.mapper

import com.example.skeletonsamples.core.data.Mapper
import com.example.skeletonsamples.skeleton.data.entity.WheelPositionEntity
import com.example.skeletonsamples.skeleton.model.WheelModel

class WheelModelMapper(
    private val tireModelMapper: TireModelMapper
) : Mapper<WheelPositionEntity, WheelModel> {

    override fun mapSingle(source: WheelPositionEntity): WheelModel =
        WheelModel(
            wheelId = source.wheelId ?: "",
            number = source.number?.trim()?.toInt() ?: 0,
            wheelType = source.type?.code ?: "",
            tire = source.tyreInformation?.let { tireModelMapper.mapSingle(it) }
        )

    override fun mapList(source: List<WheelPositionEntity>?): List<WheelModel> {
        return source?.let { wheelsList ->
            val sortedList = wheelsList.sortedWith(compareBy({ it.type?.code }, { it.number }))
            super.mapList(sortedList)
        }
            ?: super.mapList(source)
    }
}