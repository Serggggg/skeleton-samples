package com.example.skeletonsamples.skeleton.data.mapper

import com.example.skeletonsamples.core.data.Mapper
import com.example.skeletonsamples.skeleton.data.entity.VehicleTypeEntity
import com.example.skeletonsamples.skeleton.model.VehicleDisplayType

// TODO: Refactor to Mapper<DomainLayer.VehicleType, VehicleDisplayType>
class VehicleDisplayTypeMapper: Mapper<VehicleTypeEntity, VehicleDisplayType> {

    override fun mapSingle(source: VehicleTypeEntity): VehicleDisplayType =
        when (source.code) {
            "V-TR" -> VehicleDisplayType.TRAILER
            else -> VehicleDisplayType.VEHICLE
        }
}