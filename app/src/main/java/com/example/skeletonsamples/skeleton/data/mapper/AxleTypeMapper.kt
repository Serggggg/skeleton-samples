package com.example.skeletonsamples.skeleton.data.mapper

import com.example.skeletonsamples.core.data.Mapper
import com.example.skeletonsamples.skeleton.data.entity.AxleTypeEntity
import com.example.skeletonsamples.skeleton.model.AxleType

// TODO: Refactor to Mapper<DomainLayer.AxleType, AxleType>
class AxleTypeMapper : Mapper<AxleTypeEntity, AxleType> {

    override fun mapSingle(source: AxleTypeEntity): AxleType = when (source.code) {
        "A-ST" -> AxleType.ROTARY
        "A-DR" -> AxleType.DRIVE
        else -> AxleType.ORDINARY
    }
}