package com.example.skeletonsamples.skeleton

import android.content.Context
import android.view.View
import com.example.skeletonsamples.skeleton.model.DisplayMode
import com.example.skeletonsamples.skeleton.model.VehicleDisplayType
import com.example.skeletonsamples.skeleton.model.VehicleModel
import com.example.skeletonsamples.skeleton.model.WheelModel
import com.example.skeletonsamples.skeleton.view.TrailerView
import com.example.skeletonsamples.skeleton.view.TruckView
import com.example.skeletonsamples.skeleton.view.WheelView

class SkeletonController(
    private val vehicleModel: VehicleModel
) {

    private val wheelModels: MutableMap<String, WheelModel> = mutableMapOf()
    private val wheelViews: MutableMap<String, WheelView> = mutableMapOf()

    init {
        createWheelsMap()
    }

    private fun createWheelsMap() {
        vehicleModel.axles.forEach { axle ->
            axle.wheels.forEach { wheel ->
                wheelModels[wheel.wheelId] = wheel
            }
        }
    }

    private fun determineScaleFactor() {
        val maxWheels = vehicleModel.axles.map { it.wheels.size }.max()
    }

    fun buildVehicleView(context: Context): View =
        when (vehicleModel.vehicleDisplayType) {
            VehicleDisplayType.TRAILER -> TrailerView(context).apply {
                addAxles(axles = vehicleModel.axles, axleViewsMap = wheelViews)
            }
            VehicleDisplayType.VEHICLE -> TruckView(context).apply {
                addFrontAxles(axles = vehicleModel.axles.take(1), axleViewsMap = wheelViews)
                addRearAxles(
                    axles = vehicleModel.axles.takeLast(vehicleModel.axles.size - 1),
                    axleViewsMap = wheelViews
                )
            }
        }

    fun refresh() {
        wheelModels.forEach { (id, wheelModel) ->
            wheelViews[id]?.updateViewData(wheelModel)
        }
    }

    fun swapDataMode() {
        wheelModels.forEach { (id, wheelModel) ->
            wheelModel.displayMode =
                when (wheelModel.displayMode) {
                    DisplayMode.GRAPHICAL -> DisplayMode.DATA
                    DisplayMode.DATA -> DisplayMode.GRAPHICAL
                }

            wheelViews[id]?.updateViewData(wheelModel)
        }
    }
}