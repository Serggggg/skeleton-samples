package com.example.skeletonsamples.skeleton.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.example.skeletonsamples.R
import com.example.skeletonsamples.skeleton.model.AxleModel
import com.example.skeletonsamples.skeleton.model.AxleType
import kotlinx.android.synthetic.main.layout_axle_line_view.view.*

class AxleLineView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.layout_axle_line_view, this)
        layoutParams = ConstraintLayout.LayoutParams(
            ConstraintLayout.LayoutParams.MATCH_PARENT,
            ConstraintLayout.LayoutParams.WRAP_CONTENT
        )
        id = View.generateViewId()
    }

    fun bindAxleData(axle: AxleModel) {
        with (wheelAxleLabelTextView) {
            text = axle.axleNumber.toString()
            background =
                when (axle.axleType) {
                    AxleType.ORDINARY -> null
                    AxleType.ROTARY -> ContextCompat.getDrawable(context,R.drawable.ic_axle_type_rotary)
                    AxleType.DRIVE -> ContextCompat.getDrawable(context, R.drawable.ic_axle_type_drive)
                }
        }
    }
}