package com.example.skeletonsamples.skeleton.data.entity

import com.google.gson.annotations.SerializedName

data class CustomerInfoEntity(
    @SerializedName("customerNumber")
    val customerNumber: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("streetAndHouseNumber")
    val streetAndHouseNumber: String?,
    @SerializedName("postalCode")
    val postalCode: String?,
    @SerializedName("city")
    val city: String?,
    @SerializedName("country")
    val country: String?
)