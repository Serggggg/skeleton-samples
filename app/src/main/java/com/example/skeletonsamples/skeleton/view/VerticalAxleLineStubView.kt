package com.example.skeletonsamples.skeleton.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.skeletonsamples.R

class VerticalAxleLineStubView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.layout_vertical_axle_stub_view, this)
        id = View.generateViewId()
    }

    companion object {

        fun create(context: Context, height: Int): VerticalAxleLineStubView =
            VerticalAxleLineStubView(context).apply {
                layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, height)
            }
    }
}