package com.example.skeletonsamples.skeleton.data.entity

import com.google.gson.annotations.SerializedName

data class WheelTypeEntity(
    @SerializedName("code")
    val code: String?,
    @SerializedName("text")
    val text: String?
)