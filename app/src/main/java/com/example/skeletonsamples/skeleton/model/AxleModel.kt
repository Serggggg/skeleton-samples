package com.example.skeletonsamples.skeleton.model

data class AxleModel(
    val axleId: String,
    val axleType: AxleType,
    val axleNumber: Int,
    val wheels: List<WheelModel> = listOf()
) {

    val wheelsLeftSide: List<WheelModel>
        get() = wheels.take(wheels.size / 2)
    val wheelsRightSide: List<WheelModel>
        get() = wheels.takeLast(wheels.size / 2)

    private fun updateWheelLabels(
        wheelModels: List<WheelModel>,
        suffixOuter: String,
        suffixInner: String
    ) {
        val useInnerWheelsIndex = wheelModels.size > 2
        wheelModels.forEachIndexed { index, wheelModel ->
            wheelModel.displayLabel = when (index) {
                0 -> "$axleNumber$suffixOuter"
                else -> "$axleNumber$suffixInner${if (useInnerWheelsIndex) index.toString() else ""}"
            }
        }
    }

    fun recalculateWheelLabels() {
        updateWheelLabels(wheelsLeftSide, LABEL_SUFFIX_OUTER_LEFT, LABEL_SUFFIX_INNER_LEFT)
        updateWheelLabels(wheelsRightSide, LABEL_SUFFIX_OUTER_RIGHT, LABEL_SUFFIX_INNER_RIGHT)
    }

    companion object {

        private const val LABEL_SUFFIX_OUTER_LEFT = "-OL"
        private const val LABEL_SUFFIX_INNER_LEFT = "-IL"
        private const val LABEL_SUFFIX_OUTER_RIGHT = "-OR"
        private const val LABEL_SUFFIX_INNER_RIGHT = "-IR"

        // First
        fun createRotaryAxleStub2(): AxleModel =
            AxleModel(
                axleId = "100000",
                axleType = AxleType.ROTARY,
                axleNumber = 1,
                wheels = listOf(
                    WheelModel(
                        wheelId = "100001",
                        number = 1,
                        wheelType = "A-TR",
                        inspectionState = InspectionState.INSPECTED,
                        tire = TireModel(
                            tireId = "100001",
                            serial = "100001",
                            status = TireStatus.NEW,
                            maintainState = MaintainState.MAINTAINED,
                            sizeMm = 256,
                            pressureBar = 12F
                        )
                    ),
                    WheelModel(
                        wheelId = "100002",
                        number = 2,
                        wheelType = "A-TR",
                        tire = TireModel(
                            tireId = "100002",
                            serial = "100002",
                            status = TireStatus.PART_WORN,
                            maintainState = MaintainState.MAINTAINED,
                            sizeMm = 256,
                            pressureBar = 10F
                        )
                    )
                )
            ).apply { recalculateWheelLabels() }

        // Second
        fun createOrdinaryAxleStub2(): AxleModel =
            AxleModel(
                axleId = "200000",
                axleType = AxleType.ORDINARY,
                axleNumber = 2,
                wheels = listOf(
                    WheelModel(
                        wheelId = "200001",
                        number = 1,
                        wheelType = "A-TR",
                        inspectionState = InspectionState.NON_INSPECTED,
                        tire = TireModel(
                            tireId = "200001",
                            serial = "200001",
                            status = TireStatus.PART_WORN,
                            maintainState = MaintainState.MAINTAINED,
                            sizeMm = 256,
                            pressureBar = 12F
                        )
                    ),
                    WheelModel(
                        wheelId = "200002",
                        number = 2,
                        wheelType = "A-TR"
//                        tire = TireModel(
//                            tireId = "200002",
//                            serial = "200002",
//                            tyreStatus = TireStatus.PART_WORN,
//                            maintainState = MaintainState.MAINTAINED,
//                            sizeMm = 256,
//                            pressureBar = 10F
//                        )
                    )
                )
            ).apply { recalculateWheelLabels() }

        // Third
        fun createOrdinaryAxleStub4(): AxleModel =
            AxleModel(
                axleId = "300000",
                axleType = AxleType.ORDINARY,
                axleNumber = 3,
                wheels = listOf(
                    WheelModel(
                        wheelId = "300001",
                        number = 1,
                        wheelType = "A-TR",
                        tire = TireModel(
                            tireId = "300001",
                            serial = "300001",
                            status = TireStatus.PART_WORN,
                            maintainState = MaintainState.MAINTAINED,
                            sizeMm = 256,
                            pressureBar = 12F
                        )
                    ),
                    WheelModel(
                        wheelId = "300002",
                        number = 2,
                        wheelType = "A-TR",
                        tire = TireModel(
                            tireId = "300002",
                            serial = "300002",
                            status = TireStatus.PART_WORN,
                            maintainState = MaintainState.MAINTAINED,
                            sizeMm = 256,
                            pressureBar = 9F
                        )
                    ),
                    WheelModel(
                        wheelId = "300003",
                        number = 3,
                        wheelType = "A-TR",
                        tire = TireModel(
                            tireId = "300003",
                            serial = "300003",
                            status = TireStatus.PART_WORN,
                            maintainState = MaintainState.MAINTAINED,
                            sizeMm = 256,
                            pressureBar = 10F
                        )
                    ),
                    WheelModel(
                        wheelId = "300004",
                        number = 4,
                        wheelType = "A-TR",
                        inspectionState = InspectionState.INSPECTED,
                        tire = TireModel(
                            tireId = "300004",
                            serial = "300004",
                            status = TireStatus.PART_WORN,
                            maintainState = MaintainState.MAINTAINED,
                            sizeMm = 256,
                            pressureBar = 11F
                        )
                    )
                )
            ).apply { recalculateWheelLabels() }

        // Fourth
        fun createDriveAxleStub4(): AxleModel =
            AxleModel(
                axleId = "400000",
                axleType = AxleType.DRIVE,
                axleNumber = 4,
                wheels = listOf(
                    WheelModel(
                        wheelId = "400001",
                        number = 1,
                        wheelType = "A-TR",
                        inspectionState = InspectionState.INSPECTED,
                        tire = TireModel(
                            tireId = "400001",
                            serial = "400001",
                            status = TireStatus.PART_WORN,
                            maintainState = MaintainState.NON_MAINTAINED,
                            sizeMm = 256,
                            pressureBar = 12F
                        )
                    ),
                    WheelModel(
                        wheelId = "400002",
                        number = 2,
                        wheelType = "A-TR",
                        tire = TireModel(
                            tireId = "400002",
                            serial = "400002",
                            status = TireStatus.PART_WORN,
                            maintainState = MaintainState.NON_MAINTAINED,
                            sizeMm = 256,
                            pressureBar = 9F
                        )
                    ),
                    WheelModel(
                        wheelId = "400003",
                        number = 3,
                        wheelType = "A-TR",
                        tire = TireModel(
                            tireId = "400003",
                            serial = "400003",
                            status = TireStatus.PART_WORN,
                            maintainState = MaintainState.MAINTAINED,
                            sizeMm = 256,
                            pressureBar = 10F
                        )
                    ),
                    WheelModel(
                        wheelId = "400004",
                        number = 4,
                        wheelType = "A-TR",
                        tire = TireModel(
                            tireId = "400004",
                            serial = "400004",
                            status = TireStatus.PART_WORN,
                            maintainState = MaintainState.MAINTAINED,
                            sizeMm = 256,
                            pressureBar = 11F
                        )
                    )
                )
            ).apply { recalculateWheelLabels() }

    }
}