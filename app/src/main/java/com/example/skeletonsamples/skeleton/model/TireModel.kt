package com.example.skeletonsamples.skeleton.model

data class TireModel(
    val tireId: String?,
    val serial: String?,
    val status: TireStatus?,
    val maintainState: MaintainState?,
    val treadDepthMiddle: Float = 0F,
    val isRegrooveAllowed: Boolean = false,
    val productCode: String = "",
    val productDescription: String = "",
    var sizeMm: Int = 0,
    var aspectRatio: Float = 0F,
    var rimDiameter: Float = 0F,
    var fuelClass: String = "",
    var gripClass: String = "",
    var noiseClass: Int = 0,
    var noiseValue: Float = 0F,
    var pressureBar: Float = 0F     // Field is absent
)