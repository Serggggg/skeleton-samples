package com.example.skeletonsamples.core.extensions

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.skeletonsamples.core.actions.SingleLiveEvent

fun <T : Any, L : LiveData<T?>> Fragment.observe(liveData: L, body: (T?) -> Unit) =
    liveData.observe(this.viewLifecycleOwner, Observer(body))

fun <T : Any, L : LiveData<T?>> AppCompatActivity.observe(liveData: L, body: (T?) -> Unit) =
    liveData.observe(this, Observer(body))

fun <A : SingleLiveEvent<Void>> Fragment.observe(actionLiveData: A, body: () -> Unit) =
    actionLiveData.observe(this.viewLifecycleOwner, Observer { body.invoke() })

fun <A : SingleLiveEvent<Void>> AppCompatActivity.observe(actionLiveData: A, body: () -> Unit) =
    actionLiveData.observe(this, Observer { body.invoke() })

infix fun <T> MutableLiveData<T>.withDefault(initialValue: T) = apply { value = initialValue }