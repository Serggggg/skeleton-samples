package com.example.skeletonsamples.core.extensions

import android.view.View
import androidx.constraintlayout.widget.ConstraintSet

fun View.connectToParentTop(constraintSet: ConstraintSet) {
    constraintSet.connect(this.id, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP)
}

fun View.connectToParentStart(constraintSet: ConstraintSet) {
    constraintSet.connect(this.id, ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START)
}

fun View.connectToParentEnd(constraintSet: ConstraintSet) {
    constraintSet.connect(this.id, ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END)
}

fun View.connectToParentBottom(constraintSet: ConstraintSet) {
    constraintSet.connect(this.id, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM)
}

fun View.centerInParentHorizontal(constraintSet: ConstraintSet) {
    this.connectToParentStart(constraintSet)
    this.connectToParentEnd(constraintSet)
}

fun View.centerInParentVertical(constraintSet: ConstraintSet) {
    this.connectToParentTop(constraintSet)
    this.connectToParentBottom(constraintSet)
}

fun View.alignHeightTo(constraintSet: ConstraintSet, alignView: View) {
    constraintSet.connect(this.id, ConstraintSet.TOP, alignView.id, ConstraintSet.TOP)
    constraintSet.connect(this.id, ConstraintSet.BOTTOM, alignView.id, ConstraintSet.BOTTOM)
}

fun View.alignWidthTo(constraintSet: ConstraintSet, alignView: View) {
    constraintSet.connect(this.id, ConstraintSet.START, alignView.id, ConstraintSet.START)
    constraintSet.connect(this.id, ConstraintSet.END, alignView.id, ConstraintSet.END)
}

fun View.putToEndOf(constraintSet: ConstraintSet, view: View, margin: Int = 0) {
    constraintSet.connect(this.id, ConstraintSet.START, view.id, ConstraintSet.END, margin)
}

fun View.putToStartOf(constraintSet: ConstraintSet, view: View, margin: Int = 0) {
    constraintSet.connect(this.id, ConstraintSet.END, view.id, ConstraintSet.START, margin)
}

fun View.putToTopOf(constraintSet: ConstraintSet, view: View, margin: Int = 0) {
    constraintSet.connect(this.id, ConstraintSet.BOTTOM, view.id, ConstraintSet.TOP, margin)
}

fun View.putToBottomOf(constraintSet: ConstraintSet, view: View, margin: Int = 0) {
    constraintSet.connect(this.id, ConstraintSet.TOP, view.id, ConstraintSet.BOTTOM, margin)
}
