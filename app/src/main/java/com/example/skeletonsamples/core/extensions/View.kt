package com.example.skeletonsamples.core.extensions

import android.app.Activity
import android.graphics.Rect
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.annotation.LayoutRes

val View.isVisible: Boolean
    get() = this.visibility == View.VISIBLE

inline var View.isInvisible: Boolean
    get() = visibility == View.INVISIBLE
    set(value) {
        visibility = if (value) View.INVISIBLE else View.VISIBLE
    }

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.hide() {
    this.visibility = View.GONE
}

fun View.disappear() {
    this.visibility = View.INVISIBLE
}

fun View.toggleView(show: Boolean) {
    if (show) this.show()
    else this.hide()
}

fun View.resetVisibility() {
    alpha = 1F
    show()
}

/**
 * Returns true if [View] is visible on screen, otherwise false
 */
fun View.isOnScreen(activity: Activity?): Boolean {
    if (!isShown) {
        return false
    }

    val displayMetrics = DisplayMetrics()
    activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)

    val actualPosition = Rect()
    getGlobalVisibleRect(actualPosition)
    val screen = Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
    return actualPosition.intersect(screen)
}

/**
 * Invoke given function after view is measured
 */
inline fun View.afterMeasured(crossinline func: () -> Unit) {
    viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            if (measuredWidth > 0 && measuredHeight > 0) {
                viewTreeObserver.removeOnGlobalLayoutListener(this)
                func.invoke()
            }
        }
    })
}

fun ViewGroup.inflate(@LayoutRes layoutRes: Int): View =
    LayoutInflater.from(context).inflate(layoutRes, this, false)
