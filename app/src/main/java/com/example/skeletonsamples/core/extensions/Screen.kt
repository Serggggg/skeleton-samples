package com.example.skeletonsamples.core.extensions

import android.content.Context
import android.graphics.Point
import android.view.WindowManager

/**
 * Convert physical pixels to dpi
 */
fun Context.pxToDp(value: Float): Float =
    value / resources.displayMetrics.density

fun Context.pxToDp(value: Int): Int =
    (value / resources.displayMetrics.density).toInt()

/**
 * Convert dpi to physical pixels
 */
fun Context.dpToPx(value: Float): Float =
    value * resources.displayMetrics.density

fun Context.dpToPx(value: Int): Int =
    (value * resources.displayMetrics.density).toInt()

/**
 * Convert sp to physical pixels
 */
fun Context.spToPx(value: Float): Float {
    val scale = resources.displayMetrics.scaledDensity
    return value * scale
}

/**
 * Gets the size of the display, in pixels.
 * @return [Pair] object where [Pair.first] is **widthPixels** and [Pair.second] is **heightPixels**
 * @see [android.view.Display.getSize]
 */
val Context.displaySize: Pair<Int, Int>
    get() {
        val windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val size = Point()
        windowManager.defaultDisplay.getSize(size)
        return size.x to size.y
    }