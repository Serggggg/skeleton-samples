package com.example.skeletonsamples.core.data

/**
 * Interface for model mappers. It provides helper methods that facilitate
 * retrieving of models from outer data source layers
 *
 * @param <S> the Source input type
 * @param <D> the Destination return type
 *
 */
interface Mapper<S, D> {

    fun mapSingle(source: S): D

    fun mapList(source: List<S>?) = source?.map { mapSingle(it) } ?: listOf()
}