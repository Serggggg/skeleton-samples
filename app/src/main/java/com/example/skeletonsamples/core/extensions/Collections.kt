package com.example.skeletonsamples.core.extensions

import android.util.SparseArray

/**
 * Extension function to check if collection is null or empty
 */
fun <T> Collection<T>?.isNullOrEmpty(): Boolean {
    return this == null || this.isEmpty()
}

/**
 * Extension function to check if array is null or empty
 */
fun <T> Array<T>?.isNullOrEmpty(): Boolean {
    return this == null || this.isEmpty()
}

/**
 * Extension function to replace all elements
 * in destination collection with elements from [source] collection
 */
fun <T> MutableCollection<T>?.replace(source: Collection<T>?) {
    source?.let {
        this?.clear()
        this?.addAll(it)
    }
}

/** Allows the use of the index operator for storing values in the collection. */
inline operator fun <T> SparseArray<T>.set(key: Int, value: T) = put(key, value)

/** Performs the given [action] for each key/value entry. */
inline fun <T> SparseArray<T>.forEach(action: (key: Int, value: T) -> Unit) {
    for (index in 0 until size()) {
        action(keyAt(index), valueAt(index))
    }
}