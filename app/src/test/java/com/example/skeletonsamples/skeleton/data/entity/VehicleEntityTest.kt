package com.example.skeletonsamples.skeleton.data.entity

import com.google.gson.Gson
import org.junit.Assert.*
import org.junit.Test

class VehicleEntityTest {

    val jsonModels = arrayOf(
        "{\"contractInfo\":{\"contractNumber\":\"1155000567\",\"distributionChannel\":\"11\",\"salesOrganisation\":\"GB01\",\"fleetScenario\":\"40\"},\"customerInfo\":{\"customerNumber\":\"2102274\",\"name\":\"W M MORRISON SUPERMARKETS PLC\",\"streetAndHouseNumber\":\"GAIN LANE,\",\"postalCode\":\"BRADFORD\",\"city\":\"BD3 7DL\",\"country\":\"GB\"},\"generalPolicyInfo\":{\"hasBreakdownCoverage\":false,\"isHpValveCapAllowed\":false,\"isValveExtensionAllowed\":false,\"isBalancingAllowed\":false,\"isGeometriccorrectionAllowed\":false,\"isGeometricCheckAllowed\":false,\"isPressureCheckAllowed\":false,\"isPressureCorrectionAllowed\":false},\"tyrePolicyByAxle\":{\"policyInfo\":[],\"policyNumber\":null},\"vehicleHeader\":{\"vehicleId\":\"300062168\",\"licensePlate\":\"T9300\",\"chassisNumber\":\"C263842\",\"manufacturer\":\"Unspecified\",\"model\":\"6X0(222)\",\"currentOdometer\":\"281580 \",\"unitOfDistance\":\"KM\",\"tyrePolicyNumber\":\"1\",\"vehicleType\":{\"code\":\"V-TR\",\"text\":\"Trailer\"},\"fleetDepot\":{\"code\":\"UK284-001-057\",\"text\":\"WM MORRISONS TRAILER POOL N\"},\"scheduleInspectionDueDate\":\"20191012\",\"previousInspectionDate\":\"20190912\",\"isDifferentSizeAllowed\":false,\"isOdometerFitted\":false},\"axles\":[{\"axleId\":\"400164666\",\"type\":{\"code\":\"A-TR\",\"text\":\"Trailer Axle\"},\"number\":\"1\",\"numberOfTires\":\"4\",\"wheelPositions\":[{\"wheelId\":\"500390519\",\"type\":{\"code\":\"W-L\",\"text\":\"Left\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"609703939\",\"serialNumber\":\"6995868372\",\"treadDepthMiddle\":\"10\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390520\",\"type\":{\"code\":\"W-R\",\"text\":\"Right\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"609839839\",\"serialNumber\":\"6988131426\",\"treadDepthMiddle\":\"11\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390521\",\"type\":{\"code\":\"W-L\",\"text\":\"Left\"},\"number\":\"2\",\"tyreInformation\":{\"tyreId\":\"609703940\",\"serialNumber\":\"6995391281\",\"treadDepthMiddle\":\"10\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390522\",\"type\":{\"code\":\"W-R\",\"text\":\"Right\"},\"number\":\"2\",\"tyreInformation\":{\"tyreId\":\"609839840\",\"serialNumber\":\"6988132128\",\"treadDepthMiddle\":\"11\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}}]},{\"axleId\":\"400164667\",\"type\":{\"code\":\"A-TR\",\"text\":\"Trailer Axle\"},\"number\":\"2\",\"numberOfTires\":\"4\",\"wheelPositions\":[{\"wheelId\":\"500390523\",\"type\":{\"code\":\"W-L\",\"text\":\"Left\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"609372028\",\"serialNumber\":\"6986872233\",\"treadDepthMiddle\":\"8\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390524\",\"type\":{\"code\":\"W-R\",\"text\":\"Right\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"609722284\",\"serialNumber\":\"6998598247\",\"treadDepthMiddle\":\"11\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390525\",\"type\":{\"code\":\"W-L\",\"text\":\"Left\"},\"number\":\"2\",\"tyreInformation\":{\"tyreId\":\"608815645\",\"serialNumber\":\"432290\",\"treadDepthMiddle\":\"8\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390526\",\"type\":{\"code\":\"W-R\",\"text\":\"Right\"},\"number\":\"2\",\"tyreInformation\":{\"tyreId\":\"609722285\",\"serialNumber\":\"6997355455\",\"treadDepthMiddle\":\"11\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}}]},{\"axleId\":\"400164668\",\"type\":{\"code\":\"A-TR\",\"text\":\"Trailer Axle\"},\"number\":\"3\",\"numberOfTires\":\"4\",\"wheelPositions\":[{\"wheelId\":\"500390527\",\"type\":{\"code\":\"W-L\",\"text\":\"Left\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"609703941\",\"serialNumber\":\"6995868378\",\"treadDepthMiddle\":\"10\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390528\",\"type\":{\"code\":\"W-R\",\"text\":\"Right\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"609839841\",\"serialNumber\":\"6988132129\",\"treadDepthMiddle\":\"11\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390529\",\"type\":{\"code\":\"W-L\",\"text\":\"Left\"},\"number\":\"2\",\"tyreInformation\":{\"tyreId\":\"609703942\",\"serialNumber\":\"6995786931\",\"treadDepthMiddle\":\"10\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390530\",\"type\":{\"code\":\"W-R\",\"text\":\"Right\"},\"number\":\"2\",\"tyreInformation\":{\"tyreId\":\"609839842\",\"serialNumber\":\"6988131442\",\"treadDepthMiddle\":\"11\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}}]}]}",
        "{\"contractInfo\":{\"contractNumber\":\"1155000567\",\"distributionChannel\":\"11\",\"salesOrganisation\":\"GB01\",\"fleetScenario\":\"40\"},\"customerInfo\":{\"customerNumber\":\"2102274\",\"name\":\"W M MORRISON SUPERMARKETS PLC\",\"streetAndHouseNumber\":\"GAIN LANE,\",\"postalCode\":\"BRADFORD\",\"city\":\"BD3 7DL\",\"country\":\"GB\"},\"generalPolicyInfo\":{\"hasBreakdownCoverage\":false,\"isHpValveCapAllowed\":false,\"isValveExtensionAllowed\":false,\"isBalancingAllowed\":false,\"isGeometriccorrectionAllowed\":false,\"isGeometricCheckAllowed\":false,\"isPressureCheckAllowed\":false,\"isPressureCorrectionAllowed\":false},\"tyrePolicyByAxle\":{\"policyInfo\":[],\"policyNumber\":null},\"vehicleHeader\":{\"vehicleId\":\"300062172\",\"licensePlate\":\"T9304\",\"chassisNumber\":\"C275757\",\"manufacturer\":\"Unspecified\",\"model\":\"6X0(222)\",\"currentOdometer\":\"987316 \",\"unitOfDistance\":\"KM\",\"tyrePolicyNumber\":\"1\",\"vehicleType\":{\"code\":\"V-TR\",\"text\":\"Trailer\"},\"fleetDepot\":{\"code\":\"UK284-001-057\",\"text\":\"WM MORRISONS TRAILER POOL N\"},\"scheduleInspectionDueDate\":\"20190829\",\"previousInspectionDate\":\"20190730\",\"isDifferentSizeAllowed\":false,\"isOdometerFitted\":false},\"axles\":[{\"axleId\":\"400164678\",\"type\":{\"code\":\"A-TR\",\"text\":\"Trailer Axle\"},\"number\":\"1\",\"numberOfTires\":\"4\",\"wheelPositions\":[{\"wheelId\":\"500390567\",\"type\":{\"code\":\"W-L\",\"text\":\"Left\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"609151314\",\"serialNumber\":\"6994928393\",\"treadDepthMiddle\":\"7\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390568\",\"type\":{\"code\":\"W-R\",\"text\":\"Right\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"609151317\",\"serialNumber\":\"6994919304\",\"treadDepthMiddle\":\"6\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390569\",\"type\":{\"code\":\"W-L\",\"text\":\"Left\"},\"number\":\"2\",\"tyreInformation\":{\"tyreId\":\"609151315\",\"serialNumber\":\"6994919306\",\"treadDepthMiddle\":\"7\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390570\",\"type\":{\"code\":\"W-R\",\"text\":\"Right\"},\"number\":\"2\",\"tyreInformation\":{\"tyreId\":\"609151316\",\"serialNumber\":\"6994919289\",\"treadDepthMiddle\":\"6\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}}]},{\"axleId\":\"400164679\",\"type\":{\"code\":\"A-TR\",\"text\":\"Trailer Axle\"},\"number\":\"2\",\"numberOfTires\":\"4\",\"wheelPositions\":[{\"wheelId\":\"500390571\",\"type\":{\"code\":\"W-L\",\"text\":\"Left\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"608805155\",\"serialNumber\":\"6994021710\",\"treadDepthMiddle\":\"10\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390572\",\"type\":{\"code\":\"W-R\",\"text\":\"Right\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"609541695\",\"serialNumber\":\"6997179735\",\"treadDepthMiddle\":\"11\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390573\",\"type\":{\"code\":\"W-L\",\"text\":\"Left\"},\"number\":\"2\",\"tyreInformation\":{\"tyreId\":\"609275428\",\"serialNumber\":\"6994919495\",\"treadDepthMiddle\":\"10\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390574\",\"type\":{\"code\":\"W-R\",\"text\":\"Right\"},\"number\":\"2\",\"tyreInformation\":{\"tyreId\":\"609685773\",\"serialNumber\":\"6995869476\",\"treadDepthMiddle\":\"11\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}}]},{\"axleId\":\"400164680\",\"type\":{\"code\":\"A-TR\",\"text\":\"Trailer Axle\"},\"number\":\"3\",\"numberOfTires\":\"4\",\"wheelPositions\":[{\"wheelId\":\"500390575\",\"type\":{\"code\":\"W-L\",\"text\":\"Left\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"609352299\",\"serialNumber\":\"6995786691\",\"treadDepthMiddle\":\"8\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390576\",\"type\":{\"code\":\"W-R\",\"text\":\"Right\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"609685774\",\"serialNumber\":\"699586036\",\"treadDepthMiddle\":\"11\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390577\",\"type\":{\"code\":\"W-L\",\"text\":\"Left\"},\"number\":\"2\",\"tyreInformation\":{\"tyreId\":\"609352298\",\"serialNumber\":\"6996330168\",\"treadDepthMiddle\":\"8\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390578\",\"type\":{\"code\":\"W-R\",\"text\":\"Right\"},\"number\":\"2\",\"tyreInformation\":{\"tyreId\":\"609685775\",\"serialNumber\":\"6995869456\",\"treadDepthMiddle\":\"11\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}}]}]}",
        "{\"contractInfo\":{\"contractNumber\":\"1155003417\",\"distributionChannel\":\"11\",\"salesOrganisation\":\"GB01\",\"fleetScenario\":\"40\"},\"customerInfo\":{\"customerNumber\":\"30074917\",\"name\":\"Air Products PLC\",\"streetAndHouseNumber\":\"Hersham Place, Molesey Road\",\"postalCode\":\"Walton-on-Thames\",\"city\":\"KT12 4RZ\",\"country\":\"GB\"},\"generalPolicyInfo\":{\"hasBreakdownCoverage\":false,\"isHpValveCapAllowed\":false,\"isValveExtensionAllowed\":false,\"isBalancingAllowed\":false,\"isGeometriccorrectionAllowed\":false,\"isGeometricCheckAllowed\":false,\"isPressureCheckAllowed\":false,\"isPressureCorrectionAllowed\":false},\"tyrePolicyByAxle\":{\"policyInfo\":[],\"policyNumber\":null},\"vehicleHeader\":{\"vehicleId\":\"301218299\",\"licensePlate\":\"T73\",\"chassisNumber\":\"T73\",\"manufacturer\":\"Unspecified\",\"model\":\"6X0 (222)\",\"unitOfDistance\":\"KM\",\"tyrePolicyNumber\":\"1\",\"vehicleType\":{\"code\":\"V-TR\",\"text\":\"Trailer\"},\"fleetDepot\":{\"code\":\"UK519-001-002\",\"text\":\"AP PACKAGED GAS\"},\"scheduleInspectionDueDate\":\"20180703\",\"previousInspectionDate\":\"20180603\",\"isTpmsFitted\":true,\"isDifferentSizeAllowed\":false,\"isOdometerFitted\":false},\"axles\":[{\"axleId\":\"402377463\",\"type\":{\"code\":\"A-TR\",\"text\":\"Trailer Axle\"},\"number\":\"1\",\"numberOfTires\":\"2\",\"wheelPositions\":[{\"wheelId\":\"504940684\",\"type\":{\"code\":\"W-L\",\"text\":\"Left\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"607727557\",\"treadDepthMiddle\":\"17\",\"status\":{\"code\":\"R\"},\"isRegrooveAllowed\":true}},{\"wheelId\":\"504940685\",\"type\":{\"code\":\"W-R\",\"text\":\"Right\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"607727558\",\"treadDepthMiddle\":\"17\",\"status\":{\"code\":\"R\"},\"isRegrooveAllowed\":true}}]},{\"axleId\":\"402377464\",\"type\":{\"code\":\"A-TR\",\"text\":\"Trailer Axle\"},\"number\":\"2\",\"numberOfTires\":\"2\",\"wheelPositions\":[{\"wheelId\":\"504940686\",\"type\":{\"code\":\"W-L\",\"text\":\"Left\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"607727559\",\"treadDepthMiddle\":\"11\",\"status\":{\"code\":\"R\"},\"isRegrooveAllowed\":true}},{\"wheelId\":\"504940687\",\"type\":{\"code\":\"W-R\",\"text\":\"Right\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"607727560\",\"treadDepthMiddle\":\"11\",\"status\":{\"code\":\"R\"},\"isRegrooveAllowed\":true}}]},{\"axleId\":\"402377465\",\"type\":{\"code\":\"A-TR\",\"text\":\"Trailer Axle\"},\"number\":\"3\",\"numberOfTires\":\"2\",\"wheelPositions\":[{\"wheelId\":\"504940688\",\"type\":{\"code\":\"W-L\",\"text\":\"Left\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"607727561\",\"treadDepthMiddle\":\"15\",\"status\":{\"code\":\"R\"},\"isRegrooveAllowed\":true}},{\"wheelId\":\"504940689\",\"type\":{\"code\":\"W-R\",\"text\":\"Right\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"607727562\",\"treadDepthMiddle\":\"12\",\"status\":{\"code\":\"R\"},\"isRegrooveAllowed\":true}}]}]}",
        "{\"contractInfo\":{\"contractNumber\":\"1155004056\",\"distributionChannel\":\"11\",\"salesOrganisation\":\"GB01\",\"fleetScenario\":\"40\"},\"customerInfo\":{\"customerNumber\":\"30111724\",\"name\":\"Harkers Transport Limited\",\"streetAndHouseNumber\":\"De Vere Building, Riverside Road\",\"postalCode\":\"Sunderland\",\"city\":\"SR5 3JG\",\"country\":\"GB\"},\"generalPolicyInfo\":{\"hasBreakdownCoverage\":false,\"isHpValveCapAllowed\":false,\"isValveExtensionAllowed\":false,\"isBalancingAllowed\":false,\"isGeometriccorrectionAllowed\":false,\"isGeometricCheckAllowed\":false,\"isPressureCheckAllowed\":false,\"isPressureCorrectionAllowed\":false},\"tyrePolicyByAxle\":{\"policyInfo\":[],\"policyNumber\":null},\"vehicleHeader\":{\"vehicleId\":\"301253497\",\"licensePlate\":\"T93\",\"chassisNumber\":\"T93\",\"manufacturer\":\"Unspecified\",\"model\":\"6X0(222)\",\"unitOfDistance\":\"KM\",\"tyrePolicyNumber\":\"1\",\"vehicleType\":{\"code\":\"V-TR\",\"text\":\"Trailer\"},\"fleetDepot\":{\"code\":\"UK588-001-001\",\"text\":\"Harkers Transport\"},\"previousInspectionDate\":\"20190801\",\"isDifferentSizeAllowed\":false,\"isOdometerFitted\":false},\"axles\":[{\"axleId\":\"402453138\",\"type\":{\"code\":\"A-TR\",\"text\":\"Trailer Axle\"},\"number\":\"1\",\"numberOfTires\":\"2\",\"wheelPositions\":[{\"wheelId\":\"505096569\",\"type\":{\"code\":\"W-L\",\"text\":\"Left\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"608013727\",\"serialNumber\":\"6009042953\",\"treadDepthMiddle\":\"7\",\"status\":{\"code\":\"R\"},\"productInfo\":{\"code\":\"570270\",\"description\":\"385/65R22.5 KMAX T 160K158L M+S\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"385\",\"aspectRatio\":\"65\",\"rimDiameter\":\"22.5\",\"fuelClass\":\"B\",\"gripClass\":\"B\",\"noiseClass\":\"2\",\"noiseValue\":\"71\"},\"isRegrooveAllowed\":false}},{\"wheelId\":\"505096570\",\"type\":{\"code\":\"W-R\",\"text\":\"Right\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"608013728\",\"serialNumber\":\"6071092077\",\"treadDepthMiddle\":\"7\",\"status\":{\"code\":\"R\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"893631\",\"description\":\"MC 385/65R22.5 160J NT242  C1\",\"brand\":{\"code\":\"084\",\"text\":\"NEXT TREAD\"},\"sizemm\":\"385\",\"aspectRatio\":\"65\",\"rimDiameter\":\"22.5\"}}}]},{\"axleId\":\"402453139\",\"type\":{\"code\":\"A-TR\",\"text\":\"Trailer Axle\"},\"number\":\"2\",\"numberOfTires\":\"2\",\"wheelPositions\":[{\"wheelId\":\"505096571\",\"type\":{\"code\":\"W-L\",\"text\":\"Left\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"608013725\",\"treadDepthMiddle\":\"5\",\"status\":{\"code\":\"R\"},\"isRegrooveAllowed\":true}},{\"wheelId\":\"505096572\",\"type\":{\"code\":\"W-R\",\"text\":\"Right\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"608013726\",\"treadDepthMiddle\":\"5\",\"status\":{\"code\":\"R\"},\"isRegrooveAllowed\":true}}]},{\"axleId\":\"402453140\",\"type\":{\"code\":\"A-TR\",\"text\":\"Trailer Axle\"},\"number\":\"3\",\"numberOfTires\":\"2\",\"wheelPositions\":[{\"wheelId\":\"505096573\",\"type\":{\"code\":\"W-L\",\"text\":\"Left\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"609052584\",\"serialNumber\":\"8900177525\",\"treadDepthMiddle\":\"13\",\"status\":{\"code\":\"R\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"890800\",\"description\":\"NT385/65R22.5 160J NTMST M+S C1\",\"brand\":{\"code\":\"084\",\"text\":\"NEXT TREAD\"},\"sizemm\":\"385\",\"aspectRatio\":\"65\",\"rimDiameter\":\"22.5\"}}},{\"wheelId\":\"505096574\",\"type\":{\"code\":\"W-R\",\"text\":\"Right\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"608013724\",\"serialNumber\":\"8900177525\",\"treadDepthMiddle\":\"13\",\"status\":{\"code\":\"R\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"890800\",\"description\":\"NT385/65R22.5 160J NTMST M+S C1\",\"brand\":{\"code\":\"084\",\"text\":\"NEXT TREAD\"},\"sizemm\":\"385\",\"aspectRatio\":\"65\",\"rimDiameter\":\"22.5\"}}}]}]}"
    )

    @Test
    fun testJsonModel0() {
        val vehicleEntity = Gson().fromJson(jsonModels[0], VehicleEntity::class.java)

        assertNotNull(vehicleEntity)
        with(vehicleEntity.contractInfo) {
            assertEquals("1155000567", contractNumber)
            assertEquals("11", distributionChannel)
            assertEquals("GB01", salesOrganisation)
            assertEquals("40", fleetScenario)
        }
        with(vehicleEntity.customerInfo) {
            assertEquals("2102274", customerNumber)
            assertEquals("W M MORRISON SUPERMARKETS PLC", name)
            assertEquals("GAIN LANE,", streetAndHouseNumber)
            assertEquals("BRADFORD", postalCode)
            assertEquals("BD3 7DL", city)
            assertEquals("GB", country)
        }
        with(vehicleEntity.generalPolicyInfo) {
            assertEquals(false, isHpValveCapAllowed)
            assertEquals(false, isValveExtensionAllowed)
            assertEquals(false, isBalancingAllowed)
            assertEquals(false, isGeometricCorrectionAllowed)
            assertEquals(false, isGeometricCheckAllowed)
            assertEquals(false, isPressureCheckAllowed)
            assertEquals(false, isPressureCorrectionAllowed)
            assertEquals(false, isHpValveCapAllowed)
        }
        with(vehicleEntity.tyrePolicyByAxle) {
            assertNull(policyNumber)
            assertTrue(policyInfoList.isEmpty())
        }
        with(vehicleEntity.vehicleHeader) {
            assertEquals("300062168", vehicleId)
            assertEquals("T9300", licensePlate)
            assertEquals("C263842", chassisNumber)
            assertEquals("Unspecified", manufacturer)
            assertEquals("6X0(222)", model)
            assertEquals("281580 ", currentOdometer)
            assertEquals("KM", unitOfDistance)
            assertEquals("1", tyrePolicyNumber)
            assertEquals(false, isOdometerFitted)
            assertEquals(false, isDifferentSizeAllowed)
            assertNull(isTpmsFitted)
            assertEquals("V-TR", vehicleType?.code)
            assertEquals("Trailer", vehicleType?.text)
            assertEquals("UK284-001-057", fleetDepot?.code)
            assertEquals("WM MORRISONS TRAILER POOL N", fleetDepot?.text)
            assertEquals("20191012", scheduleInspectionDueDate)
            assertEquals("20190912", previousInspectionDate)
        }
        assertEquals(3, vehicleEntity.axles.size)
        with(vehicleEntity.axles[0]) {
            assertEquals("400164666", axleId)
            assertEquals("A-TR", type?.code)
            assertEquals("Trailer Axle", type?.text)
            assertEquals("1", number)
            assertEquals("4", numberOfTires)
            assertEquals(4, wheelPositions.size)

            with(wheelPositions[0]) {
                assertEquals("500390519", wheelId)
                assertEquals("1", number)
                assertEquals("W-L", type?.code)
                assertEquals("Left", type?.text)
                assertNotNull(tyreInformation)
                assertEquals("609703939", tyreInformation?.tyreId)
                assertEquals("6995868372", tyreInformation?.serialNumber)
                assertEquals("10", tyreInformation?.treadDepthMiddle)
                assertEquals("N", tyreInformation?.tyreStatus?.code)
                assertEquals(true, tyreInformation?.isRegrooveAllowed)
                assertNotNull(tyreInformation?.productInfo)
                assertEquals("570292", tyreInformation?.productInfo?.code)
                assertEquals("215/75R17.5 KMAX T 135/133J 3PSF", tyreInformation?.productInfo?.description)
                assertEquals("010", tyreInformation?.productInfo?.brand?.code)
                assertEquals("GOODYEAR", tyreInformation?.productInfo?.brand?.text)
                assertEquals("215", tyreInformation?.productInfo?.sizeMm)
                assertEquals("75", tyreInformation?.productInfo?.aspectRatio)
                assertEquals("17.5", tyreInformation?.productInfo?.rimDiameter)
                assertEquals("C", tyreInformation?.productInfo?.fuelClass)
                assertEquals("B", tyreInformation?.productInfo?.gripClass)
                assertEquals("1", tyreInformation?.productInfo?.noiseClass)
                assertEquals("67", tyreInformation?.productInfo?.noiseValue)
            }
            with(wheelPositions[1]) {
                assertEquals("500390520", wheelId)
                assertEquals("1", number)
                assertEquals("W-R", type?.code)
                assertEquals("Right", type?.text)
                assertNotNull(tyreInformation)
                assertEquals("609839839", tyreInformation?.tyreId)
                assertEquals("6988131426", tyreInformation?.serialNumber)
                assertEquals("11", tyreInformation?.treadDepthMiddle)
                assertEquals("N", tyreInformation?.tyreStatus?.code)
                assertEquals(true, tyreInformation?.isRegrooveAllowed)
                assertNotNull(tyreInformation?.productInfo)
                assertEquals("570292", tyreInformation?.productInfo?.code)
                assertEquals("215/75R17.5 KMAX T 135/133J 3PSF", tyreInformation?.productInfo?.description)
                assertEquals("010", tyreInformation?.productInfo?.brand?.code)
                assertEquals("GOODYEAR", tyreInformation?.productInfo?.brand?.text)
                assertEquals("215", tyreInformation?.productInfo?.sizeMm)
                assertEquals("75", tyreInformation?.productInfo?.aspectRatio)
                assertEquals("17.5", tyreInformation?.productInfo?.rimDiameter)
                assertEquals("C", tyreInformation?.productInfo?.fuelClass)
                assertEquals("B", tyreInformation?.productInfo?.gripClass)
                assertEquals("1", tyreInformation?.productInfo?.noiseClass)
                assertEquals("67", tyreInformation?.productInfo?.noiseValue)
            }
            with(wheelPositions[2]) {
                assertEquals("500390521", wheelId)
                assertEquals("2", number)
                assertEquals("W-L", type?.code)
                assertEquals("Left", type?.text)
                assertNotNull(tyreInformation)
                assertEquals("609703940", tyreInformation?.tyreId)
                assertEquals("6995391281", tyreInformation?.serialNumber)
                assertEquals("10", tyreInformation?.treadDepthMiddle)
                assertEquals("N", tyreInformation?.tyreStatus?.code)
                assertEquals(true, tyreInformation?.isRegrooveAllowed)
                assertNotNull(tyreInformation?.productInfo)
                assertEquals("570292", tyreInformation?.productInfo?.code)
                assertEquals("215/75R17.5 KMAX T 135/133J 3PSF", tyreInformation?.productInfo?.description)
                assertEquals("010", tyreInformation?.productInfo?.brand?.code)
                assertEquals("GOODYEAR", tyreInformation?.productInfo?.brand?.text)
                assertEquals("215", tyreInformation?.productInfo?.sizeMm)
                assertEquals("75", tyreInformation?.productInfo?.aspectRatio)
                assertEquals("17.5", tyreInformation?.productInfo?.rimDiameter)
                assertEquals("C", tyreInformation?.productInfo?.fuelClass)
                assertEquals("B", tyreInformation?.productInfo?.gripClass)
                assertEquals("1", tyreInformation?.productInfo?.noiseClass)
                assertEquals("67", tyreInformation?.productInfo?.noiseValue)
            }
            with(wheelPositions[3]) {
                assertEquals("500390522", wheelId)
                assertEquals("2", number)
                assertEquals("W-R", type?.code)
                assertEquals("Right", type?.text)
                assertNotNull(tyreInformation)
                assertEquals("609839840", tyreInformation?.tyreId)
                assertEquals("6988132128", tyreInformation?.serialNumber)
                assertEquals("11", tyreInformation?.treadDepthMiddle)
                assertEquals("N", tyreInformation?.tyreStatus?.code)
                assertEquals(true, tyreInformation?.isRegrooveAllowed)
                assertNotNull(tyreInformation?.productInfo)
                assertEquals("570292", tyreInformation?.productInfo?.code)
                assertEquals("215/75R17.5 KMAX T 135/133J 3PSF", tyreInformation?.productInfo?.description)
                assertEquals("010", tyreInformation?.productInfo?.brand?.code)
                assertEquals("GOODYEAR", tyreInformation?.productInfo?.brand?.text)
                assertEquals("215", tyreInformation?.productInfo?.sizeMm)
                assertEquals("75", tyreInformation?.productInfo?.aspectRatio)
                assertEquals("17.5", tyreInformation?.productInfo?.rimDiameter)
                assertEquals("C", tyreInformation?.productInfo?.fuelClass)
                assertEquals("B", tyreInformation?.productInfo?.gripClass)
                assertEquals("1", tyreInformation?.productInfo?.noiseClass)
                assertEquals("67", tyreInformation?.productInfo?.noiseValue)
            }
        }
    }

    @Test
    fun testJsonModel2() {
        val vehicleEntity = Gson().fromJson(jsonModels[2], VehicleEntity::class.java)

        assertNotNull(vehicleEntity)
        with(vehicleEntity.contractInfo) {
            assertEquals("1155003417", contractNumber)
            assertEquals("11", distributionChannel)
            assertEquals("GB01", salesOrganisation)
            assertEquals("40", fleetScenario)
        }
        with(vehicleEntity.customerInfo) {
            assertEquals("30074917", customerNumber)
            assertEquals("Air Products PLC", name)
            assertEquals("Hersham Place, Molesey Road", streetAndHouseNumber)
            assertEquals("Walton-on-Thames", postalCode)
            assertEquals("KT12 4RZ", city)
            assertEquals("GB", country)
        }
        with(vehicleEntity.generalPolicyInfo) {
            assertEquals(false, isHpValveCapAllowed)
            assertEquals(false, isValveExtensionAllowed)
            assertEquals(false, isBalancingAllowed)
            assertEquals(false, isGeometricCorrectionAllowed)
            assertEquals(false, isGeometricCheckAllowed)
            assertEquals(false, isPressureCheckAllowed)
            assertEquals(false, isPressureCorrectionAllowed)
            assertEquals(false, isHpValveCapAllowed)
        }
        with(vehicleEntity.tyrePolicyByAxle) {
            assertNull(policyNumber)
            assertTrue(policyInfoList.isEmpty())
        }
        with(vehicleEntity.vehicleHeader) {
            assertEquals("301218299", vehicleId)
            assertEquals("T73", licensePlate)
            assertEquals("T73", chassisNumber)
            assertEquals("Unspecified", manufacturer)
            assertEquals("6X0 (222)", model)
            assertNull(currentOdometer)
            assertEquals("KM", unitOfDistance)
            assertEquals("1", tyrePolicyNumber)
            assertEquals(false, isOdometerFitted)
            assertEquals(false, isDifferentSizeAllowed)
            assertEquals(true, isTpmsFitted)
            assertEquals("V-TR", vehicleType?.code)
            assertEquals("Trailer", vehicleType?.text)
            assertEquals("UK519-001-002", fleetDepot?.code)
            assertEquals("AP PACKAGED GAS", fleetDepot?.text)
            assertEquals("20180703", scheduleInspectionDueDate)
            assertEquals("20180603", previousInspectionDate)
        }
        assertEquals(3, vehicleEntity.axles.size)
        with(vehicleEntity.axles[0]) {
            assertEquals("402377463", axleId)
            assertEquals("A-TR", type?.code)
            assertEquals("Trailer Axle", type?.text)
            assertEquals("1", number)
            assertEquals("2", numberOfTires)
            assertEquals(2, wheelPositions.size)

            with(wheelPositions[0]) {
                assertEquals("504940684", wheelId)
                assertEquals("1", number)
                assertEquals("W-L", type?.code)
                assertEquals("Left", type?.text)
                assertNotNull(tyreInformation)
                assertEquals("607727557", tyreInformation?.tyreId)
                assertNull(tyreInformation?.serialNumber)
                assertEquals("17", tyreInformation?.treadDepthMiddle)
                assertEquals("R", tyreInformation?.tyreStatus?.code)
                assertEquals(true, tyreInformation?.isRegrooveAllowed)
                assertNull(tyreInformation?.productInfo)
            }
            with(wheelPositions[1]) {
                assertEquals("504940685", wheelId)
                assertEquals("1", number)
                assertEquals("W-R", type?.code)
                assertEquals("Right", type?.text)
                assertNotNull(tyreInformation)
                assertEquals("607727558", tyreInformation?.tyreId)
                assertNull(tyreInformation?.serialNumber)
                assertEquals("17", tyreInformation?.treadDepthMiddle)
                assertEquals("R", tyreInformation?.tyreStatus?.code)
                assertEquals(true, tyreInformation?.isRegrooveAllowed)
                assertNull(tyreInformation?.productInfo)
            }
        }
    }

}