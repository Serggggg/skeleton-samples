package com.example.skeletonsamples.skeleton.data.mapper

import com.example.skeletonsamples.skeleton.data.entity.VehicleEntity
import com.example.skeletonsamples.skeleton.model.*
import com.google.gson.Gson
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class VehicleModelMapperTest {

    private lateinit var vehicleDisplayTypeMapper: VehicleDisplayTypeMapper
    private lateinit var axleTypeMapper: AxleTypeMapper
    private lateinit var tireStatusMapper: TireStatusMapper
    private lateinit var maintainStateMapper: MaintainStateMapper
    private lateinit var tireModelMapper: TireModelMapper
    private lateinit var wheelModelMapper: WheelModelMapper
    private lateinit var axleModelMapper: AxleModelMapper
    private lateinit var vehicleModelMapper: VehicleModelMapper

    private val vehicleEntityJson = "{\"contractInfo\":{\"contractNumber\":\"1155000567\",\"distributionChannel\":\"11\",\"salesOrganisation\":\"GB01\",\"fleetScenario\":\"40\"},\"customerInfo\":{\"customerNumber\":\"2102274\",\"name\":\"W M MORRISON SUPERMARKETS PLC\",\"streetAndHouseNumber\":\"GAIN LANE,\",\"postalCode\":\"BRADFORD\",\"city\":\"BD3 7DL\",\"country\":\"GB\"},\"generalPolicyInfo\":{\"hasBreakdownCoverage\":false,\"isHpValveCapAllowed\":false,\"isValveExtensionAllowed\":false,\"isBalancingAllowed\":false,\"isGeometriccorrectionAllowed\":false,\"isGeometricCheckAllowed\":false,\"isPressureCheckAllowed\":false,\"isPressureCorrectionAllowed\":false},\"tyrePolicyByAxle\":{\"policyInfo\":[],\"policyNumber\":null},\"vehicleHeader\":{\"vehicleId\":\"300062168\",\"licensePlate\":\"T9300\",\"chassisNumber\":\"C263842\",\"manufacturer\":\"Unspecified\",\"model\":\"6X0(222)\",\"currentOdometer\":\"281580 \",\"unitOfDistance\":\"KM\",\"tyrePolicyNumber\":\"1\",\"vehicleType\":{\"code\":\"V-TR\",\"text\":\"Trailer\"},\"fleetDepot\":{\"code\":\"UK284-001-057\",\"text\":\"WM MORRISONS TRAILER POOL N\"},\"scheduleInspectionDueDate\":\"20191012\",\"previousInspectionDate\":\"20190912\",\"isDifferentSizeAllowed\":false,\"isOdometerFitted\":false},\"axles\":[{\"axleId\":\"400164666\",\"type\":{\"code\":\"A-TR\",\"text\":\"Trailer Axle\"},\"number\":\"1\",\"numberOfTires\":\"4\",\"wheelPositions\":[{\"wheelId\":\"500390519\",\"type\":{\"code\":\"W-L\",\"text\":\"Left\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"609703939\",\"serialNumber\":\"6995868372\",\"treadDepthMiddle\":\"10\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390520\",\"type\":{\"code\":\"W-R\",\"text\":\"Right\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"609839839\",\"serialNumber\":\"6988131426\",\"treadDepthMiddle\":\"11\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390521\",\"type\":{\"code\":\"W-L\",\"text\":\"Left\"},\"number\":\"2\",\"tyreInformation\":{\"tyreId\":\"609703940\",\"serialNumber\":\"6995391281\",\"treadDepthMiddle\":\"10\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390522\",\"type\":{\"code\":\"W-R\",\"text\":\"Right\"},\"number\":\"2\",\"tyreInformation\":{\"tyreId\":\"609839840\",\"serialNumber\":\"6988132128\",\"treadDepthMiddle\":\"11\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}}]},{\"axleId\":\"400164667\",\"type\":{\"code\":\"A-TR\",\"text\":\"Trailer Axle\"},\"number\":\"2\",\"numberOfTires\":\"4\",\"wheelPositions\":[{\"wheelId\":\"500390523\",\"type\":{\"code\":\"W-L\",\"text\":\"Left\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"609372028\",\"serialNumber\":\"6986872233\",\"treadDepthMiddle\":\"8\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390524\",\"type\":{\"code\":\"W-R\",\"text\":\"Right\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"609722284\",\"serialNumber\":\"6998598247\",\"treadDepthMiddle\":\"11\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390525\",\"type\":{\"code\":\"W-L\",\"text\":\"Left\"},\"number\":\"2\",\"tyreInformation\":{\"tyreId\":\"608815645\",\"serialNumber\":\"432290\",\"treadDepthMiddle\":\"8\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390526\",\"type\":{\"code\":\"W-R\",\"text\":\"Right\"},\"number\":\"2\",\"tyreInformation\":{\"tyreId\":\"609722285\",\"serialNumber\":\"6997355455\",\"treadDepthMiddle\":\"11\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}}]},{\"axleId\":\"400164668\",\"type\":{\"code\":\"A-TR\",\"text\":\"Trailer Axle\"},\"number\":\"3\",\"numberOfTires\":\"4\",\"wheelPositions\":[{\"wheelId\":\"500390527\",\"type\":{\"code\":\"W-L\",\"text\":\"Left\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"609703941\",\"serialNumber\":\"6995868378\",\"treadDepthMiddle\":\"10\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390528\",\"type\":{\"code\":\"W-R\",\"text\":\"Right\"},\"number\":\"1\",\"tyreInformation\":{\"tyreId\":\"609839841\",\"serialNumber\":\"6988132129\",\"treadDepthMiddle\":\"11\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390529\",\"type\":{\"code\":\"W-L\",\"text\":\"Left\"},\"number\":\"2\",\"tyreInformation\":{\"tyreId\":\"609703942\",\"serialNumber\":\"6995786931\",\"treadDepthMiddle\":\"10\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}},{\"wheelId\":\"500390530\",\"type\":{\"code\":\"W-R\",\"text\":\"Right\"},\"number\":\"2\",\"tyreInformation\":{\"tyreId\":\"609839842\",\"serialNumber\":\"6988131442\",\"treadDepthMiddle\":\"11\",\"status\":{\"code\":\"N\"},\"isRegrooveAllowed\":true,\"productInfo\":{\"code\":\"570292\",\"description\":\"215/75R17.5 KMAX T 135/133J 3PSF\",\"brand\":{\"code\":\"010\",\"text\":\"GOODYEAR\"},\"sizemm\":\"215\",\"aspectRatio\":\"75\",\"rimDiameter\":\"17.5\",\"fuelClass\":\"C\",\"gripClass\":\"B\",\"noiseClass\":\"1\",\"noiseValue\":\"67\"}}}]}]}"

    @Before
    fun init() {
        vehicleDisplayTypeMapper = VehicleDisplayTypeMapper()
        axleTypeMapper = AxleTypeMapper()
        tireStatusMapper = TireStatusMapper()
        maintainStateMapper = MaintainStateMapper()
        tireModelMapper = TireModelMapper(tireStatusMapper, maintainStateMapper)
        wheelModelMapper = WheelModelMapper(tireModelMapper)
        axleModelMapper = AxleModelMapper(axleTypeMapper, wheelModelMapper)
        vehicleModelMapper = VehicleModelMapper(vehicleDisplayTypeMapper, axleModelMapper)
    }

    @Test
    fun mapTest() {

        val vehicleEntity = Gson().fromJson(vehicleEntityJson, VehicleEntity::class.java)
        val vehicleModel = vehicleModelMapper.mapSingle(vehicleEntity)

        assertNotNull(vehicleModel)
        assertTrue(vehicleModel.axles.isNotEmpty())
        vehicleModel.axles.forEach { it.recalculateWheelLabels() }

        assertEquals("300062168", vehicleModel.vehicleId)
        assertEquals(VehicleDisplayType.TRAILER, vehicleModel.vehicleDisplayType)
        assertEquals(3, vehicleModel.axles.size)
        with(vehicleModel.axles[0]) {
            assertEquals("400164666", axleId)
            assertEquals(AxleType.ORDINARY, axleType)
            assertEquals(1, axleNumber)
            assertEquals(4, wheels.size)
            with(wheels[0]) {
                assertEquals("500390519", wheelId)
                assertEquals("W-L", wheelType)
                assertEquals(1, number)
                assertEquals("1-OL", displayLabel)
                assertEquals(DisplayMode.GRAPHICAL, displayMode)
                assertEquals(InspectionState.NON_INSPECTED, inspectionState)
                assertNotNull(tire)
                tire?.let {
                    assertEquals("609703939", it.tireId)
                    assertEquals("6995868372", it.serial)
                    assertEquals(TireStatus.NEW, it.status)
                    assertEquals(MaintainState.MAINTAINED, it.maintainState)
                    assertEquals(10F, it.treadDepthMiddle)
                    assertEquals(true, it.isRegrooveAllowed)
                    assertEquals("570292", it.productCode)
                    assertEquals("215/75R17.5 KMAX T 135/133J 3PSF", it.productDescription)
                    assertEquals(215, it.sizeMm)
                    assertEquals(75F, it.aspectRatio)
                    assertEquals(17.5F, it.rimDiameter)
                    assertEquals("C", it.fuelClass)
                    assertEquals("B", it.gripClass)
                    assertEquals(1, it.noiseClass)
                    assertEquals(67F, it.noiseValue)
                    assertEquals(0F, it.pressureBar)
                }
            }
            with(wheels[1]) {
                assertEquals("500390521", wheelId)
                assertEquals("W-L", wheelType)
                assertEquals(2, number)
                assertEquals("1-IL", displayLabel)
                assertEquals(DisplayMode.GRAPHICAL, displayMode)
                assertEquals(InspectionState.NON_INSPECTED, inspectionState)
                assertNotNull(tire)
                tire?.let {
                    assertEquals("609703940", it.tireId)
                    assertEquals("6995391281", it.serial)
                    assertEquals(TireStatus.NEW, it.status)
                    assertEquals(MaintainState.MAINTAINED, it.maintainState)
                    assertEquals(10F, it.treadDepthMiddle)
                    assertEquals(true, it.isRegrooveAllowed)
                    assertEquals("570292", it.productCode)
                    assertEquals("215/75R17.5 KMAX T 135/133J 3PSF", it.productDescription)
                    assertEquals(215, it.sizeMm)
                    assertEquals(75F, it.aspectRatio)
                    assertEquals(17.5F, it.rimDiameter)
                    assertEquals("C", it.fuelClass)
                    assertEquals("B", it.gripClass)
                    assertEquals(1, it.noiseClass)
                    assertEquals(67F, it.noiseValue)
                    assertEquals(0F, it.pressureBar)
                }
            }
            with(wheels[2]) {
                assertEquals("500390520", wheelId)
                assertEquals("W-R", wheelType)
                assertEquals(1, number)
                assertEquals("1-OR", displayLabel)
                assertEquals(DisplayMode.GRAPHICAL, displayMode)
                assertEquals(InspectionState.NON_INSPECTED, inspectionState)
                assertNotNull(tire)
                tire?.let {
                    assertEquals("609839839", it.tireId)
                    assertEquals("6988131426", it.serial)
                    assertEquals(TireStatus.NEW, it.status)
                    assertEquals(MaintainState.MAINTAINED, it.maintainState)
                    assertEquals(11F, it.treadDepthMiddle)
                    assertEquals(true, it.isRegrooveAllowed)
                    assertEquals("570292", it.productCode)
                    assertEquals("215/75R17.5 KMAX T 135/133J 3PSF", it.productDescription)
                    assertEquals(215, it.sizeMm)
                    assertEquals(75F, it.aspectRatio)
                    assertEquals(17.5F, it.rimDiameter)
                    assertEquals("C", it.fuelClass)
                    assertEquals("B", it.gripClass)
                    assertEquals(1, it.noiseClass)
                    assertEquals(67F, it.noiseValue)
                    assertEquals(0F, it.pressureBar)
                }
            }
            with(wheels[3]) {
                assertEquals("500390522", wheelId)
                assertEquals("W-R", wheelType)
                assertEquals(2, number)
                assertEquals("1-IR", displayLabel)
                assertEquals(DisplayMode.GRAPHICAL, displayMode)
                assertEquals(InspectionState.NON_INSPECTED, inspectionState)
                assertNotNull(tire)
                tire?.let {
                    assertEquals("609839840", it.tireId)
                    assertEquals("6988132128", it.serial)
                    assertEquals(TireStatus.NEW, it.status)
                    assertEquals(MaintainState.MAINTAINED, it.maintainState)
                    assertEquals(11F, it.treadDepthMiddle)
                    assertEquals(true, it.isRegrooveAllowed)
                    assertEquals("570292", it.productCode)
                    assertEquals("215/75R17.5 KMAX T 135/133J 3PSF", it.productDescription)
                    assertEquals(215, it.sizeMm)
                    assertEquals(75F, it.aspectRatio)
                    assertEquals(17.5F, it.rimDiameter)
                    assertEquals("C", it.fuelClass)
                    assertEquals("B", it.gripClass)
                    assertEquals(1, it.noiseClass)
                    assertEquals(67F, it.noiseValue)
                    assertEquals(0F, it.pressureBar)
                }
            }
        }
    }
}